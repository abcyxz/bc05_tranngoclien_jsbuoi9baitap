var dsnv = new danhSachNV();
var xacthuc = new xacThuc();

function getEle(id) {
  return document.getElementById(id);
}
getLocalStorage();

function layThongTinTuForm() {
  //lấy dữ liệu từ form

  var taiKhoan = getEle("tknv").value;
  var hoVaTen = getEle("name").value;
  var eMail = getEle("email").value;
  var matKhau = getEle("password").value;
  var ngayLam = getEle("datepicker").value;
  var luongCB = getEle("luongCB").value.replace(/\,/g, "");;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  var isValid = true;

  isValid &=
    xacthuc.checkXacThuc(
      taiKhoan,
      "tbTKNV",
      "Tài khoản hợp lệ từ 6-20 ký tự. (Bao gồm số 0-9, chữ IN HOA và chữ thường. Ko được có khoảng trắng và kí tự đặc biệt.)"
    ) &&
    xacthuc.checkSoKiTu(
      taiKhoan,
      "tbTKNV",
      "Tài khoản hợp lệ từ 6-20 ký tự (bao gồm số 0-9, chữ IN HOA và chữ thường).<br>Không được có khoảng trắng và ký tự đặc biệt.",
      6,
      20
    ) &&
    xacthuc.checkTaiKhoan(
      taiKhoan,
      "tbTKNV",
      "Tài khoản đã có người sử dụng, vui lòng nhập tài khoản khác.<br>Tài khoản hợp lệ từ 6-20 ký tự (bao gồm số 0-9, chữ IN HOA và chữ thường. Không được có khoảng trắng và ký tự đặc biệt.",
      dsnv.danhSach
    ) &&
    xacthuc.checkKhoangTrong(
      taiKhoan,
      "tbTKNV",
      "Tài khoản không được có khoảng trắng."
    ) &&
    xacthuc.checkKiTuDacBiet(
      taiKhoan,
      "tbTKNV",
      "Tài khoản không được có ký tự đặc biệt"
    );

  isValid &=
    xacthuc.checkXacThuc(
      hoVaTen,
      "tbTen",
      "Vui lòng nhập đầy đủ họ và tên. Chỉ nhập chữ IN HOA và chữ thường, ko nhập số và ký tự đặc biệt)."
    ) &&
    xacthuc.checkKiTu(
      hoVaTen,
      "tbTen",
      "Chỉ nhập chữ IN HOA và chữ thường, ko nhập số và ký tự đặc biệt"
    );

  isValid &=
    xacthuc.checkXacThuc(
      eMail,
      "tbEmail",
      "Email sẽ tự khởi động khởi tạo theo tài khoản của bạn. Email sẽ có định dạng: tàikhoản@cybersoft.vn"
    ) &&
    xacthuc.checkMail(
      eMail,
      "tbEmail",
      "Email sẽ tự khởi động khởi tạo theo tài khoản của bạn. Email sẽ có định dạng: tàikhoản@cybersoft.vn"
    );

  isValid &=
    xacthuc.checkXacThuc(
      matKhau,
      "tbMatKhau",
      "Vui lòng nhập mật khẩu từ 6 đến 20 ký tự. (Bao gồm 1 chữ viết IN HOA, 1 chữ số và 1 kí tự đặc biệt )"
    ) &&
    xacthuc.checkSoKiTu(
      matKhau,
      "tbMatKhau",
      "Mật khẩu phải từ 6-20 ký tự. (Bao gồm 1 chữ viết IN HOA, 1 chữ số và 1 ký tự đặc biệt ",
      6,
      20
    ) &&
    xacthuc.checkMatKhau(
      matKhau,
      "tbMatKhau",
      "Mật khẩu phải bao gồm 1 chữ viết IN HOA, 1 chữ số và 1 ký tự đặc biệt"
    );
  isValid &=
    xacthuc.checkXacThuc(
      ngayLam,
      "tbNgay",
      "Vui lòng nhập ngày bắt đầu làm việc."
    ) &&
    xacthuc.checkNgay(ngayLam, "tbNgay", "Định dạng Ngày/Tháng/Năm ko hợp lệ.");
  isValid &=
    xacthuc.checkXacThuc(
      luongCB,
      "tbLuongCB",
      "Vui lòng nhập lương cơ bản. (Chỉ nhập số, ko nhập chữ hoặc kí tự đặc biệt) "
    ) &&
    xacthuc.checkSo(
      luongCB,
      "tbLuongCB",
      "Chỉ nhập số, ko nhập chữ hoặc kí tự đặc biệt"
    );

  isValid &= xacthuc.checkChucVu(
    "chucvu",
    "tbChucVu",
    "Vui lòng chọn chức vụ trong công ty."
  );
  isValid &=
    xacthuc.checkXacThuc(
      gioLam,
      "tbGiolam",
      "Vui lòng nhập số giờ làm trong tháng. (Chỉ nhập số, ko nhập chữ hoặc kí tự đặc biệt)."
    ) &&
    xacthuc.checkSo(
      gioLam,
      "tbGiolam",
      "Chỉ nhập số, không nhập chữ hoặc ký tự đặc biệt."
    );

  if (isValid == true) {
    var nhanVien = new NhanVien(
      taiKhoan,
      hoVaTen,
      eMail,
      matKhau,
      ngayLam,
      luongCB,
      chucVu,
      gioLam
    );
    return nhanVien;
  }
  return null;
}

getEle("btnThemNV").addEventListener("click", function (event) {
  event.preventDefault();
  var nhanVien = layThongTinTuForm(true);
  if (nhanVien) {
    nhanVien.tongluong();
    nhanVien.rank();
    dsnv.addNV(nhanVien);
    showThongTinLenForm(dsnv.danhSach);
    setLocalStorage();
  }
});

function showThongTinLenForm(dsnv) {
  getEle("tableDanhSach").innerHTML = "";

  for (var i = 0; i < dsnv.length; i++) {
    var tagTr = document.createElement("tr");

    var tagTd_TaiKhoan = document.createElement("td");
    var tagTd_HoVaTen = document.createElement("td");
    var tagTd_eMail = document.createElement("td");
    var tagTd_NgayLam = document.createElement("td");
    var tagTd_ChucVu = document.createElement("td");
    var tagTd_TongLuong = document.createElement("td");
    var tagTd_XepLoai = document.createElement("td");
    var tagTd_BtnDel = document.createElement("td");
    var tagTd_BtnEdit = document.createElement("td");

    tagTd_TaiKhoan.innerHTML = dsnv[i].tk;
    tagTd_HoVaTen.innerHTML = dsnv[i].ten;
    tagTd_eMail.innerHTML = dsnv[i].email;
    tagTd_NgayLam.innerHTML = dsnv[i].ngaylam;
    tagTd_ChucVu.innerHTML = dsnv[i].chucvu;
    tagTd_TongLuong.innerHTML = dsnv[i].tongLuong.toLocaleString();
    tagTd_XepLoai.innerHTML = dsnv[i].xeploai;
    tagTd_BtnEdit.innerHTML =
      '<button data-toggle="modal" data-target="#myModal" class="btn btn-primary" onclick="editNV(\'' +
      dsnv[i].tk +
      '\')">Sửa</button>';
    tagTd_BtnDel.innerHTML =
      '<button class="btn btn-danger" onclick="deleteNV(\'' +
      dsnv[i].tk +
      '\')">Xóa</button>';

    tagTr.appendChild(tagTd_TaiKhoan);
    tagTr.appendChild(tagTd_HoVaTen);
    tagTr.appendChild(tagTd_eMail);
    tagTr.appendChild(tagTd_NgayLam);
    tagTr.appendChild(tagTd_ChucVu);
    tagTr.appendChild(tagTd_TongLuong);
    tagTr.appendChild(tagTd_XepLoai);
    tagTr.appendChild(tagTd_BtnEdit);
    tagTr.appendChild(tagTd_BtnDel);

    getEle("tableDanhSach").appendChild(tagTr);
  }
}

function deleteNV(tk) {
/*   console.log('yes')
 */  var aus = confirm('Bạn có chắc muốn xóa TK"' + tk + '" không?');
  if (aus == true) {
    dsnv.xoaTaiKhoan(tk);
    showThongTinLenForm(dsnv.danhSach);
    setLocalStorage();
  } else {
  }
}

function editNV(tk) {
  var thongTin = dsnv.timThongTin(tk);
  getEle("tknv").value = thongTin.tk;
  getEle("tbTKNV").innerHTML = "Không thể thay đổi TK";
  alertUpdate("tbTKNV");
   getEle("tknv").disabled = true; 
  getEle("name").value = thongTin.ten;
  getEle("email").value = thongTin.email;
  getEle("email").disabled = true;
  getEle("tbEmail").innerHTML = "Không thể thay đổi email";
  alertUpdate("tbEmail");
  getEle("password").value = thongTin.matkhau;
  getEle("datepicker").value = thongTin.ngaylam;
  getEle("luongCB").value = thongTin.luong;
  getEle("chucvu").value = thongTin.chucvu;
  getEle("gioLam").value = thongTin.giolam;
  getEle("btnCapNhat").style.display = "inline-block";
  getEle("btnThemNV").style.display = "none";
}

getEle("btnCapNhat").addEventListener("click", function (event) {
  event.preventDefault();
  var NhanVien = layThongTinTuForm(false);
  NhanVien.tongluong();
  NhanVien.rank();
  dsnv.capNhatNV(NhanVien);
  showThongTinLenForm(dsnv.danhSach);
  setLocalStorage();
  resetValidation();
});

getEle("btnThem").addEventListener("click", function () {
  getEle("btnCapNhat").style.display = "none";
  getEle("btnThemNV").style.display = "inline-block";
  resetValidation();
});

getEle("btnDong").addEventListener("click", function () {
  resetForm();
});

function resetForm() {
  getEle("formInfoEmployee").reset();
  getEle("tknv").disabled = false;
  resetValidation();
}
function setLocalStorage() {
  var jsondsnv = JSON.stringify(dsnv.danhSach);
  localStorage.setItem("danhsachNV", jsondsnv);
}

function getLocalStorage() {
  if (localStorage.getItem("danhsachNV")) {
    dsnv.danhSach = JSON.parse(localStorage.getItem("danhsachNV"));
    showThongTinLenForm(dsnv.danhSach);
  }
}

getEle("btnTimNV").addEventListener("keyup", function () {
  var keyword = getEle("btnTimNV").value;
  var arrSearch = dsnv.timkiemNV(keyword);
  showThongTinLenForm(arrSearch);
});

function format(input) {
  var nStr = input.value + "";
  nStr = nStr.replace(/\,/g, "");
  x = nStr.split(".");
  x1 = x[0];
  x2 = x.length > 1 ? "." + x[1] : "";
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, "$1" + "," + "$2");
  }
  input.value = x1 + x2;
}

function autoFillEmail() {
  getEle("email").value = getEle("tknv").value + "@cybersoft.vn";
}

function alertUpdate(spanID) {
  getEle(spanID).style.display = "block";
  getEle(spanID).className = "alert-danger";
  getEle(spanID).style.fontSize = "11px";
  getEle(spanID).style.color = "red";
  getEle(spanID).style.padding = "0.3rem";
  getEle(spanID).style.display = "block";
}

function resetValidation() {
  getEle("tbTKNV").style.display = "none";
  getEle("tbTen").style.display = "none";
  getEle("tbEmail").style.display = "none";
  getEle("tbMatKhau").style.display = "none";
  getEle("tbNgay").style.display = "none";
  getEle("tbLuongCB").style.display = "none";
  getEle("tbChucVu").style.display = "none";
  getEle("tbGiolam").style.display = "none";
}
