function getEle(id){
    return document.getElementById(id);
}

function NhanVien (taiKhoan, hoVaTen, 
    eMail, matKhau,
    ngayLam,luongCB,chucVu, gioLam){
    this.tk = taiKhoan;
    this.ten = hoVaTen;
    this.email = eMail;
    this.matkhau = matKhau;
    this.ngaylam = ngayLam;
    this.luong  = luongCB;
    this.chucvu = chucVu;
    this.giolam = gioLam;
    this.tongLuong = 0;
    this.xeploai = "";

    this.rank = function() {
        if(this.chucvu == "Nhân viên"){
            if (this.giolam >= 192){
                return this.xeploai = "Xuất Sắc";
            }
            if (this.giolam >= 176){
                return this.xeploai = "Giỏi";
            }
            if (this.giolam >= 160){
                return this.xeploai = "Khá";
            }
            return this.xeploai = "Trung Bình";
        }
    };

    this.tongluong = function () {
        if(this.chucvu == "Sếp" ){
            return this.tongLuong = parseFloat(this.luong)*3;
        }
        if(this.chucvu == "Trưởng phòng") {
            return this.tongLuong = parseFloat(this.luong)*2;

        }
        if (this.chucvu == "Nhân viên") {
          return this.tongLuong = parseFloat(this.luong)*1;
        }
    };
}

//Cú pháp: parseFloat( string): phân tích chuỗi

